@section("content")
    <div class="intro-slider-container">
        <div class="owl-carousel owl-simple owl-light owl-nav-inside" data-toggle="owl" data-owl-options='{"nav": false}'>
            @foreach(\Modules\Slider\Entities\Slider::all()->where('active', true) as $slider)
            <div class="intro-slide" style="background-image: url(/images/slider/{{ $slider->src_images }});">
                <div class="container intro-content">
                    <h3 class="intro-subtitle">{{ $slider->title_images }}</h3><!-- End .h3 intro-subtitle -->
                    <h1 class="intro-title">{!! $slider->text_images !!}</h1><!-- End .intro-title -->

                    @if($slider->button)
                    <a href="{{ $slider->button_link }}" class="{{ $slider->button_class }}">
                        {!! $slider->button_text !!}
                    </a>
                    @endif
                </div><!-- End .container intro-content -->
            </div><!-- End .intro-slide -->
            @endforeach
        </div><!-- End .owl-carousel owl-simple -->

        <span class="slider-loader text-white"></span><!-- End .slider-loader -->
    </div><!-- End .intro-slider-container -->
@endsection
