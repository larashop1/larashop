<?php

use App\Model\Core\Country;
use App\Model\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Core\Shop::create([
            "name" => "Probioairsante",
            "adresse" => "22 Rue Maryse Bastié",
            "code_postal" => "85100",
            "ville" => "Les Sables d'Olonne",
            "email" => "contact@probioairsante.shop",
            "phone" => "0899 492 648"
        ]);

        \App\Model\Core\ShopBank::create([
            "shop_id" => 1,
            "bank_name" => "N26",
            "bank_adresse" => "N26 Bank",
            "iban" => "DE93 1001 1001 2627 1026 84",
            "bic" => "NTSBDEB1XXX"
        ]);

        \App\Model\Core\ShopSocial::create([
            "shop_id" => 1
        ]);

        Country::insert([
            ['name' => 'France', 'tax' => 0.2],
            ['name' => 'Belgique', 'tax' => 0.2],
            ['name' => 'Suisse', 'tax' => 0],
            ['name' => 'Canada', 'tax' => 0],
        ]);

        factory(User::class, 20)
            ->create()
            ->each(function ($user) {
                $user->addresses()->createMany(
                    factory(\App\Model\UserAdresse::class, mt_rand(2, 3))->make()->toArray()
                );
            });
        $user = User::find(1);
        $user->admin = true;
    }
}
