<?php

namespace App\Providers;

use App\Model\Core\ConfigSeo;
use App\Model\Core\Shop;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(!$this->app->runningInConsole()) {
            View::share('shop', Shop::firstOrFail());
            View::share('seo', ConfigSeo::firstOrFail());
        }
    }
}
