<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserAdresse extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
