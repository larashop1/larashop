<?php

namespace App\Model\Core;

use Illuminate\Database\Eloquent\Model;

class ShopSocial extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
