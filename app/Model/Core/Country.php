<?php

namespace App\Model\Core;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
