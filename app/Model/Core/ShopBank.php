<?php

namespace App\Model\Core;

use Illuminate\Database\Eloquent\Model;

class ShopBank extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
