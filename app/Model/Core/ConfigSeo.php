<?php

namespace App\Model\Core;

use Illuminate\Database\Eloquent\Model;

class ConfigSeo extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
