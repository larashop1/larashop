<?php

namespace App\Model\Core;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $guarded = [];
    public $timestamps = false;

    public function bank()
    {
        return $this->hasOne(ShopBank::class);
    }

    public function social()
    {
        return $this->hasOne(ShopSocial::class);
    }
}
